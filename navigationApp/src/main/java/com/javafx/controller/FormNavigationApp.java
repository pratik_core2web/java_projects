package com.javafx.controller;


import com.javafx.dataAccess.FormPage1;
import com.javafx.dataAccess.FormPage2;
import com.javafx.dataAccess.FormPage3;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FormNavigationApp extends Application {

    private Stage pStage;
    private Scene page1Scene,page2Scene,page3Scene;
    
    private FormPage1 page1;
    private FormPage2 page2;
    private FormPage3 page3;

    @Override
    public void start(Stage pStage) {
        this.pStage = pStage;

        page1 = new FormPage1(this);
        page2 = new FormPage2(this);
        page3 = new FormPage3(this);

        page1Scene = new Scene(page1.getView(),700,500);
        page2Scene = new Scene(page2.getView(),700,500);
        page3Scene = new Scene(page3.getView(),700,500);
        pStage.setScene(page1Scene);
        pStage.setTitle("GoogleForm");
        pStage.setResizable(true);
        pStage.show();
    }

    public void navigateToPage1(){
        page2.setField2Value(page2.getField2Value());
        page1.setField1Value(page1.getField1Value());
        pStage.setScene(page1Scene);
    }

    public void navigateToPage2(){
        page1.setField1Value(page1.getField1Value());
        page3.setField3Value(page3.getField3Value());
        pStage.setScene(page2Scene);
    } 

    public void navigateToPage3(){
        page2.setField2Value(page2.getField2Value());
        pStage.setScene(page3Scene);
    } 

}
