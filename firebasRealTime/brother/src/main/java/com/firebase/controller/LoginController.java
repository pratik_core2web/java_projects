package com.firebase.controller;

import  com.firebase.firebaseConnection.FirebaseService;
import java.io.FileInputStream;
import java.io.IOException;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
//import com.google.firebase.FirebaseOptions.Builder;
//import com.google.firebase.internal.FirebaseService;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class LoginController extends Application {

    private Stage primaryStage;
    private FirebaseService firebaseService;
    private Scene scene;
   
    public void setPrimaryStageScene(Scene scene){
        primaryStage.setScene(scene);
    }
    
    public void initializeLoginScene() {
        Scene loginScene = createLoginScene();
        this.scene = loginScene;
        primaryStage.setScene(loginScene);
    }
    
    @Override
    public void start(Stage primaryStage){
        this.primaryStage = primaryStage;

        //initialize firebase app
        try{
            FileInputStream serviceAccount = new FileInputStream("brother\\src\\main\\resources\\pratik-fb-firebase-adminsdk-1s6yc-3388684359.json");

            @SuppressWarnings("deprecation")
            FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://pratik-fb-default-rtdb.asia-southeast1.firebasedatabase.app")
                .build();

            FirebaseApp.initializeApp(options);
        }
        catch(IOException e){
            e.printStackTrace();
        }

        //create initial login scene

        Scene scene = createLoginScene();
        this.scene = scene;
        primaryStage.setTitle("Firibase Auth");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
        
    /**
     * create the initial login scene)
     * @return the scene object representing login ui
     */

    private Scene createLoginScene(){
        
        TextField emailField = new TextField();
        emailField.setPromptText("Email");

        PasswordField passwordField = new PasswordField();
        passwordField.setPromptText("Password");

        Button signUpButton = new Button("Sign UP");
        Button logInButton = new Button("Log in");

        firebaseService = new FirebaseService(this,emailField,passwordField);

        signUpButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                firebaseService.signUp();
            }
        });

        logInButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                firebaseService.login();
            }
        });

        VBox fieldBox = new VBox(20,emailField,passwordField);
        HBox buttonBox = new HBox(20,logInButton,signUpButton);
        VBox comBox = new VBox(10,fieldBox,buttonBox);
        Pane viewPane = new Pane(comBox);

        return new Scene(viewPane,400,200);
























        
    } 






























}
