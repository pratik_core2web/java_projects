package com.firebase.firebaseConnection;

import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.io.OutputStream;

import org.json.JSONObject;
import com.firebase.controller.LoginController;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;

import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
//import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.VBox;
import javafx.scene.control.Label;

public class FirebaseService {

    private TextField emailField;
    private PasswordField passwordField;
    private LoginController loginController;

    public FirebaseService(LoginController loginController,TextField emailField,PasswordField passwordField){
        this.loginController = loginController;
        this.emailField = emailField;
        this.passwordField = passwordField;
    }

    public boolean signUp(){
        String email = emailField.getText();
        String password = passwordField.getText();

        try {
            UserRecord.CreateRequest request = new UserRecord.CreateRequest()
                .setEmail(email)
                .setPassword(password)
                .setDisabled(false);

            UserRecord userRecord = FirebaseAuth.getInstance().createUser(request);
            System.out.println("Succesfully created user"+ userRecord.getUid());
            showAlert("Success","User Created Succesfully");
            return true;           

        } catch (FirebaseAuthException e) {
            e.printStackTrace();
            showAlert("Error", "Failed to create user: "+ e.getMessage());
            return false;
    
        }

    }

    public boolean login(){
        String email = emailField.getText();
        String password = passwordField.getText();

        try{

            String apiKey = "AIzaSyCk0XAxfOLEbjEv89b7HYl7qe9oHvBuXzo";

            URL url = new URL("https:/ /identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=" + apiKey);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type","application/json; charset=UTF-8");
            conn.setDoOutput(true);


            JSONObject jsonrequest = new JSONObject();
            jsonrequest.put("email",email);
            jsonrequest.put("password", password);
            jsonrequest.put("returnSecureToken",true);

            try(OutputStream os = conn.getOutputStream()){
                byte[] input = jsonrequest.toString().getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }

            if(conn.getResponseCode() == 200){
                showAlert(true);
                return true;
            } else{
                showAlert("Invalid login","Invalid credentials..!");
                return false;
            }

        } catch(Exception e){
            e.printStackTrace();
            showAlert(false);
            return false;
        }
    }
    

    private void showAlert(String title, String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private void showAlert(boolean isLoggedIn){

        Label msg = new Label("Welcome");
        msg.setAlignment(Pos.CENTER);

        Button logOutButton = new Button("LOGOUT");

        VBox vb = new VBox(100,msg,logOutButton);

        logOutButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               loginController.initializeLoginScene();
            }
        });

        Scene scene = new Scene(vb,400,200);
        loginController.setPrimaryStageScene(scene);
    }
}