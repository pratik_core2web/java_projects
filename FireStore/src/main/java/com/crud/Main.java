package com.crud;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import com.crud.FireBaseConfig.FirebaseInit;

public class Main {
    public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
        FirebaseInit obj = new FirebaseInit();
        FirebaseInit.initializeFirebase();
        
        obj.createRec();
        obj.readRec();
        obj.updateRec();
    }
}