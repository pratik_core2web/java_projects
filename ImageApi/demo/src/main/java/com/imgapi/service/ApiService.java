package com.imgapi.service;

import java.io.IOException;

import org.json.JSONObject;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class ApiService extends Application {

    static String imgURL = " " ;

    public static void imageData() throws IOException{

        StringBuffer response = new DataUrls().getResponsibleData();

        if(response != null){

            JSONObject obj = new JSONObject(response.toString());
            JSONObject urlObject = obj.getJSONObject("urls");
            
            imgURL = urlObject.getString("small");
        }

        else{
            System.out.println("Response is empty");
        }
    }

    @Override
    public void start(Stage pStage) throws Exception {
        imageData();

        Image img = new Image(imgURL);

        ImageView iv = new ImageView(img);

        Pane imgPane = new Pane();
        imgPane.getChildren().add(iv);

        Scene sc = new Scene(imgPane,img.getWidth(),img.getHeight());
        pStage.setScene(sc);

        pStage.setTitle("ImageView Example");
        pStage.show();
    }
    
}
