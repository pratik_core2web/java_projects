package com.form.dataAccess;

import com.form.controller.FormNavigationApp;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;

public class FormPage1 {
    private FormNavigationApp app;
    private GridPane view;
    private TextField field1;
    

    public FormPage1(FormNavigationApp app){
        this.app = app;
        initialize();
    }


    private void initialize(){
        view = new GridPane();
        view.setPadding(new Insets(30));
        view.setHgap(20);
        view.setVgap(20);

        Label l1 = new Label("Field-1:");

        field1 = new TextField();

        Button nextButton = new Button("Next");
        nextButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.navigateToPage2();
            }
            
        });

        Button clearButton = new Button("Clear");
        clearButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                setField1Value(null);
            }           
        });
        
         Image img = new Image("images/web.jpeg");

         BackgroundImage bgImage = new BackgroundImage(
            img, 
            BackgroundRepeat.NO_REPEAT, 
            BackgroundRepeat.NO_REPEAT, 
            BackgroundPosition.CENTER,
            new BackgroundSize(
                BackgroundSize.AUTO, 
                BackgroundSize.AUTO, 
                //700,
                //500,
                false, 
                false, 
                true, 
                false
            )
        );

        view.add(l1,0,0);
        view.add(field1,1,0);
        view.add(nextButton,1,1);
        view.add(clearButton,2,1);
        view.setPrefSize(500, 400);
        //view.setStyle("-fx-background-image: url(images/web.jpeg);-fx-background-repeat: no-repeat");
        view.setBackground(new Background(bgImage));
    }

    public GridPane getView() {
        return view;
    }

    public String getField1Value(){
        return field1.getText();
    }

    public void setField1Value(String value){
        field1.setText(value);
    }    
}
